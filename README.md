# Product List Code review

Simple list of products to illustrate basic filtering and layout in React.

## Getting started

Pull the master branch, go to the root of the project in your terminal of choice and run either `yarn install` or `npm install`.

Once installed, run `yarn start` or `npm start` and then visit http://localhost:8080/ in your browser.

Shop for Women's tops at your leisure.

### Notes

- The product names in the data were longer than the design would suit, at this point I would have had a chat with the designer to look at an amended design because I tried moving price underneath when the title got too long it looked weird/inconsistent. For this test I altered the layout to something sensible.
- As requested, I didn't use any boilerplate for this but included a _super_ basic Webpack config as it was more work to write decent code than without it. I also included an `.eslintrc` config file to keep the code looking squeaky clean.
- The layout will not work in older browsers, in a production setting I would have of course added fallbacks.

### Built with

- [React](https://reactjs.org/)
- [Styled Components](https://www.styled-components.com/)
