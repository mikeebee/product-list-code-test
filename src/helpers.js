import { css } from 'styled-components';

/**
 * Simple breakpoint helpers
 *
 * Use like this:
 *
 * ${breakpoint.small`
 *   margin-top: 0;
 * `};
 *
 */

export const breakpoint = {
  small: (...args) => css`
    @media (min-width: 600px) {
      ${css(...args)};
    }
  `,
};

