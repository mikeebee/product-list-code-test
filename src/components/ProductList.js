import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Product from './Product';

const ProductList = ({ products }) => (
  <StyledProductList>
    {products.map(item => (
      <li key={item.index}>
        <Product {...item} />
      </li>
    ))}
  </StyledProductList>
);

ProductList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
};

// Styled components
const StyledProductList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  display: grid;
  grid-auto-rows: minmax(450px, auto);
  grid-gap: 0;
  grid-template-columns: repeat(auto-fill, minmax(310px, 1fr));

  li {
    border: 1px solid #e1e1e1;
    display: flex;

    /* Hide the borders on following items */
    margin-bottom: -1px;
    margin-right: -1px;
  }
`;

export default ProductList;
