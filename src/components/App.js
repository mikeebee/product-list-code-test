/**
 * Main Products View
 */

import React, { Component } from 'react';

// Data
import productsData from '../data/products.json';

// Components
import Container from './Container';
import ProductList from './ProductList';
import Header from './Header';

class App extends Component {
  state = {
    products: productsData,
    currentSize: null,
  };

  getSizesFromProductData = () => {
    // Reference array to dicate the correct order of the sizes
    const desiredSizesOrder = ['XS', 'S', 'M', 'L', 'XL'];

    // Get the sizes, flatten the array & remove duplicates
    const allSizes = productsData
      .map(product => product.size)
      .reduce((sizes, currentSize) => [...new Set(sizes.concat(currentSize))], []);

    // Order the list based on the desiredSizesOrder reference list
    return desiredSizesOrder.filter(size => allSizes.includes(size));
  };

  filterListBySize = (size) => {
    // Get the size from the filter dropdown
    const currentSize = size.target.value;

    return this.setState({
      products: !currentSize
        ? productsData
        : productsData.filter(product => product.size.includes(currentSize)),
      currentSize,
    });
  };

  render() {
    const { currentSize, products } = this.state;

    return (
      <Container>
        <Header
          title="Women's tops"
          sizes={this.getSizesFromProductData()}
          onFilterChange={this.filterListBySize}
          currentSize={currentSize}
        />
        <main>
          <ProductList products={products} />
        </main>
      </Container>
    );
  }
}

export default App;
