import styled from 'styled-components';

const Container = styled.div`
  color: #333;
  font-family: 'Helvetica Neue', arial, sans-serif;
  margin: 0 auto;
  max-width: 1320px;
`;

export default Container;
