import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { breakpoint } from '../helpers';

const Header = ({
  title, onFilterChange, sizes, currentSize,
}) => (
  <StyledHeader>
    <h1>{title}</h1>
    <form onChange={onFilterChange}>
      <Label htmlFor="filter">Filter by size</Label>
      <StyledSelect name="filter" id="filter">
        <option value="">{!currentSize ? 'Filter by size' : 'Show all'}</option>
        {sizes.map(option => (
          <option value={option} key={option}>
            {option}
          </option>
        ))}
      </StyledSelect>
    </form>
  </StyledHeader>
);

Header.propTypes = {
  onFilterChange: PropTypes.func.isRequired,
  sizes: PropTypes.arrayOf(PropTypes.string).isRequired,
  title: PropTypes.string.isRequired,
  currentSize: PropTypes.string,
};

Header.defaultProps = {
  currentSize: null,
};

// Styled components
const StyledHeader = styled.header`
  align-items: center;
  background-color: #def0f3;
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
  margin-top: 3rem;
  padding: 0.6rem 1rem;

  ${breakpoint.small`
    flex-direction: row;
 `};

  h1 {
    margin: 0;
    text-align: center;
    font-size: 28px;

    ${breakpoint.small`
      flex-direction: row;
      font-size: 38px;
      text-align: left;
    `};
  }

  form {
    margin-top: 1rem;

    ${breakpoint.small`
      margin-left: auto;
      margin-top: 0;
   `};
  }
`;

const StyledSelect = styled.select`
  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0);
  font-size: 18px;
  min-width: 200px;
  transition: box-shadow 0.2s ease-in-out;

  /* Improve default focus style */
  &:focus {
    outline: none;
    box-shadow: 0 0 0 4px rgba(170, 206, 214, 0.8);
  }
`;

const Label = styled.label`
  /* https://snook.ca/archives/html_and_css/hiding-content-for-accessibility */
  position: absolute !important;
  height: 1px;
  width: 1px;
  overflow: hidden;
  clip: rect(1px, 1px, 1px, 1px);
`;

export default Header;
