import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { breakpoint } from '../helpers';

const Product = ({
  productImage, productName, price, isSale, isExclusive,
}) => (
  <StyledProduct>
    <header>
      <figure>
        <img src={`/assets/products/${productImage}`} alt={productName} />
      </figure>
      {isSale && <ItemNote>Sale</ItemNote>}
      {isExclusive && <ItemNote exclusive>Exclusive</ItemNote>}
    </header>
    <footer>
      <div>
        <h2>{productName}</h2>
        <p>{price}</p>
      </div>
    </footer>
  </StyledProduct>
);

Product.propTypes = {
  isSale: PropTypes.bool.isRequired,
  isExclusive: PropTypes.bool.isRequired,
  price: PropTypes.string.isRequired,
  productImage: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
};

// Styled components
const StyledProduct = styled.article`
  display: flex;
  flex-direction: column;
  max-height: 100%;
  justify-content: center;
  padding: 2rem 1rem;
  width: 100%;

  header {
    figure {
      margin: 0;
      padding: 0;
      position: relative;

      &:before {
        content: 'Loading image';
        color: #aaa;
        display: block;
        font-size: 12px;
        left: 50%;
        position: absolute;
        top: 50%;
        transform: translate3d(-50%, -50%, 0);
        z-index: 0;
      }
    }

    img {
      height: 280px;
      width: 280px;
      display: block;
      margin: 0 auto;
      position: relative;
    }
  }

  h2 {
    flex-grow: 1;
    font-size: 22px;
    line-height: 1.2;
    margin: 0 0 0.5rem;

    ${breakpoint.small`
      font-size: 26px;
   `};

    /* Price  */
    + p {
      font-size: 28px;
      font-weight: bold;
      margin-left: auto;
      margin: 0;

      ${breakpoint.small`
        font-size: 36px;
     `};
    }
  }

  footer {
    margin-top: auto;
    text-align: center;

    div {
      align-items: flex-end;
    }
  }
`;

const ItemNote = styled.span`
  background-color: #cb3233;
  color: #fff;
  display: inline-block;
  margin-bottom: 2rem;
  margin-top: 1rem;
  padding: 1rem 1.2rem;

  /* Modifier - Exclusive */
  ${props => props.exclusive
    && `
    background-color: #009900;
    padding: 1rem 2rem;
  `};
`;

export default Product;
